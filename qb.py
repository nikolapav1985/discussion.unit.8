#!/usr/bin/python

"""
FILE qb.py

exception examples (use standard input to read a file)

RUN SCRIPT EXAMPLE

./qb.py < div

INPUT FILE EXAMPLE

5
2

OUTPUT EXAMPLE

2

INPUT FILE EXAMPLE (BAD)

5
0

OUTPUT EXAMPLE (EXCEPTION)

Cannot divide by zero.
"""

import sys

if __name__ == "__main__":
    items = []

    for line in sys.stdin:
        """
            assume all items are integers
        """
        items.append(int(line.rstrip()))

    try:
        """
            try to perform integer division. it is going to fail if second item is zero.
        """
        print(items[0]//items[1])
    except ZeroDivisionError:
        """
            handle division by zero exception. print message and exit script (enable error code).
        """
        print("Cannot divide by zero.")
        exit(1)

