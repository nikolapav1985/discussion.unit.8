Discussion forum unit 8
-----------------------

- qa.py (example ValueError exception)
- qb.py (example ZeroDivisionError exception)
- qc.py (example AssertionError exception)

Details
-------

Check each file comments for details (included examples to run scripts).

Test environment
----------------

os lubuntu 16.04 lts 4.13.0 kernel version 2.7.12 python version
