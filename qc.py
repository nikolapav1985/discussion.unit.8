#!/usr/bin/python

"""
FILE qc.py

exception examples (use standard input to read a file)

RUN SCRIPT EXAMPLE

./qc.py < assert

INPUT FILE EXAMPLE

1
2
3
4
5

OUTPUT EXAMPLE

[1, 2, 3, 4, 5]

INPUT FILE EXAMPLE (BAD, THE FILE IS EMPTY)

OUTPUT EXAMPLE (EXCEPTION)

There are zero items in the list.
"""

import sys

if __name__ == "__main__":
    items = []

    for line in sys.stdin:
        """
            assume all items are integers
        """
        items.append(int(line.rstrip()))

    try:
        assert len(items) != 0
    except AssertionError:
        """
            there are no items in the list. print message and exit script.
        """
        print("There are zero items in the list.")
        exit(1)

    """
        there are items in the list. all is good, continue script.
    """
    print(items)


