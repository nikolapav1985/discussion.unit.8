#!/usr/bin/python

"""
FILE qa.py

file read exceptions (use standard input)

RUN SCRIPT EXAMPLE

./qa.py < int

INPUT FILE EXAMPLE

1
2
3

OUTPUT EXAMPLE

[1, 2, 3]

INPUT FILE EXAMPLE (BAD)

abc
def
ghi

OUTPUT EXAMPLE (EXCEPTION)

Could not convert data to integer.

"""

import sys

if __name__ == "__main__":
    items = []

    for line in sys.stdin:
        """
            add items to a list. try to convert items to integers. if fail, catch exception and print message. to actually fix this, try to use a file that contains appropriate values. to detect error it is possible to check program output, logs or exit code.
        """
        try:
            items.append(int(line.rstrip()))
        except ValueError:
            print("Could not convert data to integer.")
            """
                could not convert data to integer. exit program and enable error code.
            """
            exit(1)

    print(items)


